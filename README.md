# Dotfiles-Mac for inox-ee

![dotfiles](./misc/imgs/dotfiles.gif)

## Environments

- OS: macOS Catalina v10.15.7
- SHELL: Zsh
  - Plugin manager: [zinit](https://github.com/zdharma/zinit)
  - Color theme: [Powerlevel10k](https://github.com/romkatv/powerlevel10k)

## How to setup

## Step 1. - Install Dotfiles

The easiest way to install dotfiles is to execute:

```sh
$ git clone https://github.com/inox-ee/dotfiles.git
$ ~/dotfiles/initial.sh
# >>> Done
$ ~/dotfiles/setup.zsh
```

This will install dotfiles and complete your own zsh environment.

## other setup

- ssh
- aws

## Caution!

1. This repository does not preserve `.zhistory`.
