#!/bin/sh

while true; do
    read -p "Do you want to run $0? (y/n)" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit 0;;
        * ) echo "Please answer yes or no.";;
    esac
done

# =====
# install homebrew
# =====
type brew > /dev/null 2>&1 || /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# =====
# apt update/upgrade
# =====
(brew upgrade)

# =====
# install zsh
# =====
type zsh > /dev/null 2>&1 || sudo brew install -q zsh

# =====
# set zsh and create ~/.zshrc
# =====
[ $SHELL == "$(which zsh)" ] || chsh -s $(which zsh)

CURRENTDIR=$(cd $(dirname $0); pwd)
ZSHENV="$HOME/.zshenv"
if ! [ -f $ZSHENV ]; then
  echo export ZDOTDIR=${CURRENTDIR}\\nsource \$ZDOTDIR/.zshenv\\n > $ZSHENV
fi

# =====
# continue to setup.zsh
# =====
echo "\e[32m[Success to install ZSH]\e[m After restart terminal, \e[30;43m please run setup.sh \e[m"

while true; do
    read -p "Do you restart terminal? (y/n)" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit 0;;
        * ) echo "Please answer yes or no.";;
    esac
done
exec zsh
