# Aliases
alias ls='gls --group-directories-first --human-readable --color -F'
alias l='ls -la'
alias rm='echo "This is not the command you are looking for."; false'
alias ln='ln -iv'
alias dc='docker-compose'
alias cl='clear'

## Ruby
alias ber='bundle exec rails'
alias berspec='bundle exec rspec'

## Python
alias pip='python3 -m pip'
alias python='python3'
alias prp='poetry run python'

## git
alias gad='git add'
alias gada='git add .'
alias gcm='git commit -m'
alias gfe='git fetch --prune'
alias gps='git push origin HEAD'
alias gsw='git switch'
alias gswd='git switch develop'
alias grsa='git restore .'

