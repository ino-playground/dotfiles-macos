# Setting some applications

# direnv
if whence -w direnv 1>/dev/null 2>&1; then
  eval "$(direnv hook zsh)"
fi

# fzf
[ -f $ZDOTDIR/.fzf.zsh ] && source $ZDOTDIR/.fzf.zsh
export FZF_DEFAULT_OPTS='--height 60% --reverse --border'

# rbenv
if whence -w rbenv 1>/dev/null 2>&1; then
  eval "$(rbenv init -)"
fi

# nvm
export NVM_DIR="/usr/local/opt/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/etc/bash_completion.d/nvm" ] && . "$NVM_DIR/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

# yarn
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

# pyenv
if whence -w pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

