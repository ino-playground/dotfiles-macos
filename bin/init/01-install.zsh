#!/bin/zsh

echo -n "Do you want to run $0?(y/N): "; read -q && echo "" || exit 0

# =====
# install zinit (skip recommended plugins)
# =====
# # you don't need to do anything.

# =====
# install pyenv
# =====
if ! [ -d $HOME/.pyenv ]; then
  git clone https://github.com/pyenv/pyenv.git $HOME/.pyenv
  (cd $HOME/.pyenv && src/configure && make -C src)
fi

# =====
# isntall pip
# =====
echo -n "$(python3 --version), is it ok?(y/N): "
if read -q && echo ""; then
  python3 -m pip install --upgrade pip
  echo $(pip --version)
else
  echo "\e[31m[CAUTION!] please install Python 3.0.0>\e[m"
fi

# =====
# install fzf
# =====
if ! [ -d $HOME/.fzf ]; then
  git clone -q --depth 1 https://github.com/junegunn/fzf.git $HOME/.fzf
  echo "\e[33m[WARNING] please enable fzf-keybinding and disable other settings.\e[m"
  $HOME/.fzf/install
  rm $HOME/.fzf.bash
  rm $HOME/.fzf.zsh
fi

# =====
# install vim-hybrid
# =====
mkdir -p ~/.vim/colors
(cd $ZDOTDIR/.vim/colors && curl -s -O https://raw.githubusercontent.com/w0ng/vim-hybrid/master/colors/hybrid.vim)

# =====
# install rbenv
# =====
if ! whence -w rbenv; then
  git clone -q https://github.com/rbenv/rbenv.git $HOME/.rbenv
  git clone -q https://github.com/sstephenson/ruby-build.git $HOME/.rbenv/plugins/ruby-build
fi

# =====
# install trash-cli
# =====
whence -w pip && whence -w trash || pip install -q trash-cli

# =====
# install tree
# =====
whence -w tree || brew install -q tree

# =====
# install expect (for `unbuffer` command)
# =====
whence -w unbuffer || brew install -q expect


# =====
# enable coreutils (`gls`, etc.)
# =====
! whence -w brew || brew install -q coreutils
